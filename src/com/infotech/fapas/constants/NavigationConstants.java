package com.infotech.fapas.constants;

import java.io.Serializable;

public class NavigationConstants implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3572133841185581960L;
	 
	public static final String FACES_REDIRECT = "?faces-redirect=true";
	
	public static final String STAGE_ACTION_PAGE = "stageAction";
	public static final String ADD_EDIT_ACTION_PAGE = "addAction";
	public static final String STAGE_TYPE_PAGE  = "stageType"; 
	public static final String ADD_EDIT_STAGE_TYPE_PAGE  = "addStageType";
	public static final String STAGE_PAGE  = "stages";
	public static final String ADD_EDIT_STAGE_PAGE  = "addEditStage";
	public static final String WORKFLOW_PAGE  = "workflows";
	public static final String ADD_EDIT_WORKFLOW_PAGE  = "addEditWorkflow";
	public static final String ADD_GROUP  = "addEditRole";
	public static final String ADD_USER  = "addEditUser";
	public static final String USER_PAGE= "users";
	public static final String GROUP_PAGE= "roles";
	public static final String ADD_EDIT_USER  = "userManagement";
	public static final String SEARCH_USER  = "searchUser";
	public static final String TRANSACTION_DETAILS_OUCTOME  = "TransactionDetails";	
	public static final String ADD_EDIT_REGISTRATION_CENTER  = "addEditregisCenters";
	public static final String ADD_REGISTRATION_CENTER  = "addEditRegisCenters";
	public static final String ADD_REGISTRAR_GENERAL= "addEditRegistrarGeneral";
	public static final String ADD_EDIT_REGISTRAR_GENERAL  = "addEditregistrarGeneral";
	public static final String REGISTRATION_CENTER_PAGE= "registrationCenters";
	public static final String REGISTRAR_GENERAL_PAGE= "registrarGeneral";
	
	public static final String VIEW_PAID_REGISTRATION_DETAILS_PAGE = "viewPaidDeathRegistrationDeatils";
	public static final String VIEW_REGISTRATION_DETAILS_PAGE = "viewDeathRegistrationDeatils";
	public static final String SEARCH_REGISTRATION_DETAILS_PAGE = "searchRegisteredDeath";
	public static final String EDIT_REGISTRATION_DETAILS_PAGE = "editDeathRegistration";
	public static final String EDIT_PAID_DEATH_REGISTRATION_DETAILS_PAGE_EXTERNAL = "editPaidDeathRegistration";
	
	public static final String VIEW_BIRTH_REGISTRATION_DETAILS_PAGE = "viewBirthRegistrationDetails";
	public static final String VIEW_PAID_BIRTH_REGISTRATION_DETAILS_PAGE = "viewPaidBirthRegistrationDetails";
	public static final String VIEW_SPECIAL_BIRTH_REGISTRATION_DETAILS_PAGE = "viewSpecialBirthRegistrationDetails";
	public static final String VIEW_SPECIAL_PAID_BIRTH_REGISTRATION_DETAILS_PAGE = "viewSpecialPaidBirthRegistrationDetails";
	
	public static final String SEARCH_BIRTH_REGISTRATION_DETAILS_PAGE = "searchRegisteredBirth";
	public static final String EDIT_OFFLINE_REJECTED_BIRTH_REGISTRATION_DETAILS_PAGE = "/pages/secure/registration/birth/editOfflineRejectedBirthRegistration";
	public static final String EDIT_OFFLINE_REJECTED_SPECIAL_BIRTH_REGISTRATION_DETAILS_PAGE = "/pages/secure/registration/birth/editOfflineRejectedSpecialBirthRegistration";
	public static final String EDIT_OFFLINE_REJECTED_DEATH_REGISTRATION_DETAILS_PAGE = "/pages/secure/registration/death/editOfflineRejectedDeathRegistration";
	
	public static final String VIEW_OFFLINE_PROCESSED_BIRTH_REGISTRATION_DETAILS_PAGE = "/pages/secure/registration/birth/viewOfflineProcessedBirthRegistration";
	public static final String VIEW_OFFLINE_PROCESSED_DEATH_REGISTRATION_DETAILS_PAGE = "/pages/secure/registration/death/viewOfflineProcessedDeathRegistration";

	public static final String PROCESSED_REGISTRATION_DETAILS_PAGE = "/pages/secure/registration/processedRegistrationList";
	
	public static final String SEARCH_PAID_BIRTH_REGISTRATION_DETAILS_PAGE = "searchPaidRegisteredBirth";
	public static final String SEARCH_PAID_DEATH_REGISTRATION_DETAILS_PAGE = "searchPaidRegisteredDeath";

	public static final String BIRTH_CERTIFICATE_PAGE = "birthCertificate";
	public static final String DEATH_CERTIFICATE_PAGE = "deathCertificate";
	
	public static final String EDIT_BIRTH_REGISTRATION_DETAILS_PAGE = "editBirthRegistration";
	public static final String EDIT_PAID_BIRTH_REGISTRATION_DETAILS_PAGE_EXTERNAL = "editPaidBirthRegistration";
	public static final String EDIT_SPECIAL_BIRTH_REGISTRATION_DETAILS_PAGE = "editSpecialBirthRegistration";
	public static final String EDIT_SPECIAL_PAID_BIRTH_REGISTRATION_DETAILS_PAGE = "editSpecialPaidBirthRegistration";
	
	public static final String EDIT_RE_REGISTRATIION_PAID_BIRTH = "editReRegistrationPaidBirth";
	public static final String VIEW_RE_REGISTRATION_PAID_BIRTH = "viewReRegistrationPaidBirthDetails";
	public static final String EDIT_RE_BIRTH_REGISTRATION = "editReBirthRegistration";
	
	public static final String ADD_TRANSACTION_TYPE  = "addEditTransactionType";	
	public static final String TRANSACTION_DEATH_TYPE_PAGE= "deathRegisType";
	public static final String ADD_BIRTH_TRANSACTION_TYPE  = "addEditBirthRegisType";
	public static final String ADD_DEATH_TRANSACTION_TYPE  = "addEditDeathRegisType";
	public static final String VIEW_BIRTH_NOTIFICATION  = "birthNotificationDetails";
	public static final String BIRTH_NOTIFICATION_PAGE= "birthNotificationList";
	public static final String VIEW_DEATH_NOTIFICATION  = "deathNotificationDetails";
	public static final String DEATH_NOTIFICATION_PAGE= "deathNotificationList";
	public static final String ADD_CERTIFICATE_ISSUED= "addEditCertificateIssue";
	public static final String CERTIFICATE_ISSUED_PAGE= "certificateIssue";
	public static final String ADD_TRANSACTION_TYPE_WAVE_FEE= "addEditTransactionWaveFee";
	public static final String TRANSACTION_TYPE_PAGE= "transactionType";
	public static final String TRANSACTION_TYPE_WAVE_FEE_PAGE= "transactionWaveFee";
	public static final String TRANSACTION_BIRTH_TYPE_PAGE= "birthRegisType";
	
	public static  String ADD_EDIT_REGION_PAGE = "addRegion";
	public static  String REGION_PAGE = "regions";
	public static  String ADD_EDIT_DISTRICT_PAGE = "addEditDistricts";
	public static final String DISTRICT_PAGE = "districts";
	
	public static  String ADD_EDIT_COUNCIL_PAGE = "addCouncil";
	public static final String COUNCIL_PAGE = "councils";
	public static  String ADD_EDIT_WARD_PAGE = "addEditWard";
	public static final String WARD_PAGE = "wards";
	public static  String ADD_EDIT_HFACILITY_PAGE = "addEditHealthFacility";
	public static final String HFACILITY_PAGE = "healthFacility";
	
	public static final String ADD_DEVICE  = "addEditDevice";
	public static final String DEVICE_PAGE= "device";
	public static final String VERIFY_PAYMENT_PAGE= "verfiyPayment";
	public static final String VERIFY_OFFLINE_PAYMENT_PAGE= "/pages/secure/trans/verfiyPayment";
	public static final String DASHBOARD = "/pages/secure/dashboard";
	public static final String PRIVILEGES_PAGE = "privileges";
	
	public static final String SECURITY_TOKEN_PAGE = "securityTokenGeneration";
	public static final String DYNAMIC_REPORTS_MAIN  = "dynamicReportMain";
	public static final String ADD_EDIT_DYNAMIC_REPORT_PAGE  = "plainQueryEditor";
	public static final String DYNAMIC_QUERY_EDITOR_PAGE  = "designQueryEditor";
	
}
