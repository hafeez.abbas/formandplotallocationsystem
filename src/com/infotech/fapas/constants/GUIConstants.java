package com.infotech.fapas.constants;

import java.io.Serializable;

public class GUIConstants implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4950689262927830860L;

	public static String SEPERATOR = "/";
	public static String SYSTEM_EXCEPTION = "systemException";

	public static final String EMPTY_STRING = "";
	public static final String SPACE = " ";
	public static final String NOT_AVAILABLE = "N/A";
	public static final String COMMA = ", ";
	public static final String DOT = ".";
	public static final boolean TRUE = true;
	public static final boolean FALSE = false;
	public static final Long ZERO_LONG = 0L;
	public static final Long ONE_LONG = 1L;
	public static final Long TWO_LONG = 2L;
	public static final Long THREE_LONG = 3L;
	public static final Long FOUR_LONG = 4L;
	public static final Long FIVE_LONG = 5L;
	public static final Integer ONE_INTEGER = 1;
	public static final Integer ZERO_INTEGER = 0;
	public static final Double ZERO_DOUBLE = 0.0;
	public static final Float ZERO_FLOAT = 0.0F;
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String YES_STRING = "Yes";
	public static final String NO_STRING = "No";
	public static final String SAVE_BUTTON = "save";
	public static final String EDIT_BUTTON = "edit";
	public static final String ROLE_ADMIN = "ADMIN";
	public static final String ROLE_SUPER_ADMIN = "SUPER_ADMIN";
	public static final String ROLE_REGISTRAR = "REGISTRAR";
	public static final String DASHBOARD_OUTCOME = "Dashboard";
	public static final String ADMIN_DASHBOARD_OUTCOME = "AdminDashboard";
	public static final String REGISTRAR_DASHBOARD_OUTCOME = "RegistrarDashboard";
	public static final String SUB_ADMIN_DASHBOARD_OUTCOME = "SubAdminDashboard";
	public static final String BACK_TO_LOGIN_OUTCOME="BackToLogin";
	
	public static final Long BIRTH_TRANSACTION_TYPE = 1L;
	public static final Long DEATH_TRANSACTION_TYPE = 2L;
	public static final Long EDIT_BIRTH_TRANSACTION_TYPE = 3L;
	public static final Long EDIT_DEATH_TRANSACTION_TYPE = 4L;
	public static final Long BIRTH_CERTIFICATE_DUPLICATE_PRINT = 5L;
	public static final Long DEATH_CERTIFICATE_DUPLICATE_PRINT = 6L;
	public static final Long RE_REG_BIRTH = 7L;
	public static final Long RE_REG_DEATH = 8L;
	
	public static final Long DEFAULT_COUNTRY = 229L;
	public static final String BIRTH_NOTIFICATION_MALE = "M";
	public static final String BIRTH_NOTIFICATION_FEMALE = "F";
	public static final String BIRTH_NOTIFICATION_MALE_STRING = "Male";
	public static final String BIRTH_NOTIFICATION_FEMALE_STRING = "Female";
	public static final Long MALE = 1L;
	public static final Long FEMALE = 2L;
	public static final Long ALIVE = 1L;
	public static final Long DEAD = 2L;
	public static final Long REVOKED = 3L;
	public static final Long HOME = 1L;
	public static final Long HEALT_FACILITY = 2L;
	public static final Long OTHER = 3L;
	public static final String NOTIFICATIN_STATUS_MARKED = "Y";
	
	public static final String fieldChangeStyle = "fieldChange";
	public static final String pictureChangeStyle = "pictureChange";
	public static final String EMAIL_SERVER_DOWN = "emailServerDown";
	
	public static final String BIRTH_REGISTATION_SUBJECT = "Birth Registration";
	public static final String BIRTH_REGISTATION = "Birth Registration";
	public static final String BIRTH_RE_REGISTATION = "Birth Re-Registration";
	public static final String BIRTH_RE_REGISTATION_SUBJECT = "Birth Re-Registration";
	public static final String SPECIAL_BIRTH_REGISTATION = "Special Birth Registration";
	public static final String SPECIAL_BIRTH_REGISTATION_SUBJECT = "Special Birth Registration";
	
	public static final String DEATH_REGISTATION_SUBJECT = "Death Registration";
	public static final String DEATH_REGISTATION = "Death Registration";
	public static final String DEATH_RE_REGISTATION = "Death Re-Registration";
	public static final String DEATH_RE_REGISTATION_SUBJECT = "Death Re-Registration";
	
	public static final String EDIT_BIRTH_REGISTATION_SUBJECT = "Birth Modification";
	public static final String EDIT_DEATH_REGISTATION_SUBJECT = "Death Modification";
	public static final String EDIT_BIRTH_REGISTATION = "Birth Modification";
	public static final String EDIT_DEATH_REGISTATION = "Death Modification";
	
	public static final String BIRTH_DUBLICATE_CERTITIFICATE_SUBJECT = "Birth Duplicate Certificate";
	public static final String BIRTH_DUBLICATE_CERTITIFICATE = "Birth Duplicate Certificate";
	public static final String DEATH_DUBLICATE_CERTITIFICATE_SUBJECT = "Death Duplicate Certificate";
	public static final String DEATH_DUBLICATE_CERTITIFICATE = "Death Duplicate Certificate";
	
	// STAGE TYPES CONSTANTS
	public static class StageTypeConstants{
		public static final Long STAGE_TYPE_VERIFY_PAYMENT = 1l;
		public static final Long STAGE_TYPE_PRINT_CERTIFICATE = 2l;
		public static final Long STAGE_TYPE_COMPLETE = 3l;
	}
	
	// STAGE TYPES CONSTANTS
	public static class StageConstants{
		public static final Long VERIFY_PAYMENT_STAGE = 1l;
		public static final Long PRINT_CERTIFICATE_STAGE = 2l;
		public static final Long COMPLETE_STAGE = 3l;
	}
	
	// STAGE TYPES CONSTANTS
	public static class DocumentTypeConstants {
		public static final Long BIRTH_REG_DOC = 1l;
		public static final Long DEATH_REG_DOC = 2l;
		public static final Long RE_BIRTH_DOC = 3l;
		public static final Long CERT_REPRINT_DOC = 4l;
		public static final Long DUP_CERT_PRINT_DOC = 5L;
		public static final Long RE_DEATH_DOC = 6L;
	}
		
	// ACTION TYPES CONSTANTS
		public static class ActionTypeConstants{
			public static Long ACTION_TYPE_OK = 1l;
			public static Long ACTION_TYPE_CANCEL = 2l;
			public static Long ACTION_TYPE_VERIFY_PAYMENT = 3l;
			public static Long ACTION_TYPE_PAYMENT_RECEIVED = 4l;
			public static Long ACTION_TYPE_PRINT_CERTIFICATE = 5l;
		}
		
	public static class LoginConstants {

		// Possible Outcomes...
		public static String ADMIN_LOGIN_SUCCESS_OUTCOME = "AdminLoginSuccess";
		public static String REGISTRAR_LOGIN_SUCCESS_OUTCOME = "RegistrarLoginSuccess";
		public static String SUB_ADMIN_LOGIN_SUCCESS_OUTCOME = "SubAdminLoginSuccess";
		public static String SUPER_ADMIN_LOGIN_SUCCESS_OUTCOME = "SuperAdminLoginSuccess";
		public static String LOGIN_FAILURE_OUTCOME = "LoginFailure";
		public static String LOGOUT_SUCCESS_OUTCOME = "LogoutSuccess";
		public static String DASHBOARD_OUTCOME = "Dashboard";

		public static String ADMIN_CHANGE_PASSWORD_OUTCOME = "AdminChangePassword";
		public static String REGISTRAR_CHANGE_PASSWORD_OUTCOME = "RegistrarChangePassword";
		public static String SUB_ADMIN_CHANGE_PASSWORD_OUTCOME = "SubAdminChangePassword";
		public static String CHANGE_PASSWORD_OUTCOME = "changePassword";
		public static String RETRIEVE_PASSWORD_EMAIL_SENT = "forgotPasswordEmailSent";
		public static String NO_USER_WITH_EMAIL = "noUserWithEmail";
		public static String USER_NOT_ASSOCIATED_OR_NO_PRIVILEGES_ASSIGNED = "userNotAssociated";
		// GUI Constants

		// Resource Bundle Constants
		public static String AUTHENTICATION_EXCEPTION = "authenticationException";
		public static String LOGOUT_EXCEPTION = "logoutException";
		public static String PASSWORD_CHANGE_SUCCESSFULL = "passwordChangedSuccessfully";
		public static String USER_ALREADY_LOGGEDIN = "userAlreadyLoggedIn";
		public static String USER_LOCKED_DUE_UNSUCCESFULL_ATTEMPTS = "userLockDueToMaximumAttempts";
		public static String USER_LOCKED = "userLock";
		public static String OLD_PASSWORD_INCORRECT = "oldPasswordIncorrect";
		public static String SAME_PASSWORD = "samePassword";
		public static String SPECIAL_CHARACTER_PASSWORD= "specialCharacterPassword";
		public static String USER_MANAGEMENT = "UserManager";
		public static String ROLE_MANAGEMENT = "RoleManager";
		public static String USER_IN_ROLE = "userInRole";
		public static String USERS_OUTCOME = "users";
		public static String USER_INACTIVE = "inActiveUser";
		public static String USER_REGISTRATION_CENTER = "userCenter";
		public static String USER_CENTER_INACTIVE = "userCenterInactive";
	}

	public static class GroupManagementConstants {

		// Resource Bundle Constants
		public static String GROUP_ADD_TOKEN = "groupAddSuccess";
		public static String GROUP_EDIT_TOKEN = "groupEditSuccess";
		public static String GROUP_ALREADY_EXISTS_TOKEN = "groupAlreadyExists";
		public static String GROUP_DELETE_TOKEN = "groupDeleteSuccess";
		public static String PRIVILEGES_UPDATED_TOKEN = "privilegesAddSuccess";		
		public static String IS_ADD_EDIT_ROLE = "isAddOrEditRole";
		public static String GROUP_PARAM = "groupId";	
		public static String GROUP_ALREADY_EXIST_WITH_LDAP = "groupIsExistWithLdap";	
		
	}

	public static class UserManagementConstants {

		// Resource Bundle Constants
		public static String USER_ADD_TOKEN = "userAddSuccess";
		public static String USER_EDIT_TOKEN = "userEditSuccess";
		public static String USER_ALREADY_EXISTS_TOKEN = "userAlreadyExists";
		public static String USER_DELETE_TOKEN = "userDeleteSuccess";
		public static String USER_LOCK_TOKEN = "userLockSuccess";
		public static String USER_UNLOCK_TOKEN = "userUnlockSuccess";
		public static String REGIS_CENTER_ALREADY_EXIST = "regisCenterAlreadyExist";
		public static String REGIS_CENTER_EDIT = "regisCenterEdit";
		public static String REGIS_CENTER_DELETE = "regisCenterDelete";
		public static String ADD_REGIS_CENTER = "regisCenterAdd";
		public static String REGIS_CENTER_COUNCIL_REQUIRED = "councilMsg";
		public static String REGIS_CENTER_WARD_REQUIRED = "wardMsg";
		public static String REGIS_CENTER_HEALTH_FACILITY_REQUIRED = "healthMsg";
		public static String REGIS_CENTER_PRINTING = "printingForRegistration";
		public static String SAVE_SYSTEM_CONFIGURATION = "saveConfiguration";
		public static String UPDATE_SYSTEM_CONFIGURATION = "updateConfiguration";
		public static String IS_ADD_EDIT_USER = "isAddOrEditUser";
		public static String USER_PARAM = "userId";
		public static String GROUP_ID_PARAM = "userGroupId";
		public static String INACTIVE_CENTER = "incativeCenterForUser";
		public static String INACTIVE_CENTER_DEVICES = "incativeCenterForDevice";
		public static String EMAIL_NOT_VALID = "emailNotValid";
		public static String EMAIL_NOT_VALID_FORMAT = "emailValidationError";
		
		public static final int USER_MANAGEMENT_ROLE = 1;
	}
	
	public static class CertificateConstants {
		public static String IS_ADD_EDIT_CERTIFICATE_ISSUED = "isAddOrEditCertificateIssued";
		public static String CERTIFICATE_ISSUED_PARAM = "certificateIssuedId";
		public static String CERTIFICATE_ISSUED_ALREADY_EXIST = "certificateIssuedAlreadyExist";
		public static String CERTIFICATE_ISSUED_RANGE_EXIST = "certificateIssuedRangeExist";
		public static String ADD_CERTIFICATE_ISSUED= "addCertificateIssued";
		public static String EDIT_CERTIFICATE_ISSUED= "editCertificateIssued";
		public static String STARTING_FROM_SERIAL= "startingFromSerial";
		public static String REGION_ID_PARAM = "regionId";
		public static String DISTRICT_ID_PARAM = "districtId";
		public static String COUNCIL_ID_PARAM = "councilId";
		public static String WARD_ID_PARAM = "wardId";
		public static String HEALTH_FACILITY_ID_PARAM = "facilityId";
		public static String REGISTRATION_CENTER_ID_PARAM = "centerId";
		public static String REGISTRATION_CENTER_TYPE_ID_PARAM = "centerTypeId";
	}
	
	public static class RegistrationConstants {

		// Resource Bundle Constants
		public static String ADD_REGISTRAR_GEN_TOKEN = "registrarAddSuccess";
		public static String EDIT_REGISTRAR_GEN_TOKEN = "registrarEditSuccess";
		public static String REGISTRAR_GEN_ALREADY_EXISTS_TOKEN = "registrarAlreadyExists";
		public static String DELETE_REGISTRAR_GEN_TOKEN = "registrarDeleteSuccess";
		public static String REGISTRAR_GEN_LOCK_TOKEN = "registrarLockSuccess";
		public static String REGISTRAR_GEN_UNLOCK_TOKEN = "registrarUnlockSuccess";
		public static String REGISTRAR_GEN_FROM_DATE = "registrarfromDate";
		public static String CURRENT_REGISTRAR_GEN_ALREADY_EXIST = "currentRegistrarExists";
		public static String CURRENT_ACTIVE = "currentActive";
		public static String DO_NOT_RESET_CURRENT = "isOverWriteRegistarar";
		
		public static String REGIS_CENTER_ALREADY_EXIST = "regisCenterAlreadyExist";
		public static String REGIS_CENTER_EDIT = "regisCenterEdit";
		public static String REGIS_CENTER_DELETE = "regisCenterDelete";
		public static String ADD_REGIS_CENTER = "regisCenterAdd";
		public static String IS_ADD_EDIT_REGISTRATION_CENTER = "isAddOrEditRegistrationCenter";
		public static String IS_ADD_EDIT_REGISTRAR_GENERAL = "isAddOrEditRegistrarGeneral";
		public static String REGISTRATION_CENTER_PARAM = "regisCenterId";
		public static String REGISTRATION_CENTER_TYPE_PARAM = "regisCenterTypeId";
		public static String REGISTRATION_CENTER_CODE_PARAM = "regisCenterCode";
		public static String REGISTRATION_CENTER_STATUS_PARAM = "regisCenterStatus";
		public static String REGISTRAR_GENERAL_PARAM = "regisGenId";
		public static String REGISTRATION_CENTER_SELECTION = "centerSelection";
		
		public static String BIRT_REGISTRATION_DETAIL_PARAM = "birthRegId";
		public static String BIRT_REGISTRATION_PREVIEW_OUTCOME = "viewBirthRegistrationDetails";
		
		public static final Long  BIRTH_TRANSACTION_TYPE = 1L;
		public static final Long  DEATH_TRANSACTION_TYPE = 2L;
		public static String TRANSACTION_DETAIL_PARAM = "transactionDTO";
		
		public static String VIEW_DEATH_REGISTRATION_NUMBER = "deathRegistrationNumber";
		public static String IS_ADD_EDIT_DEATH_REGISTRATION_DETAILS = "isAddOrEditDeathRegistration";
		public static String IS_CERTIFICATE_PAGE = "isCertificatePage";
		public static String EDIT_OFFLINE_REJECTED_BIRTH_ID = "editOfflineBirthId";
		public static String EDIT_OFFLINE_REJECTED_DEATH_ID = "editOfflineDeathId";
		
		public static String VIEW_OFFLINE_REJECTED_BIRTH_ID = "viewOfflineBirthId";
		public static String VIEW_OFFLINE_REJECTED_DEATH_ID = "viewOfflineDeathId";
		
		public static String VIEW_BIRTH_REGISTRATION_NUMBER = "birthRegistrationNumber";
		public static String IS_ADD_EDIT_BIRTH_REGISTRATION_DETAILS = "isAddOrEditBirthRegistration";
		
		public static final Long  E_NOTIFICATION_TYPE = 1L;
		public static final Long  OTHER_NOTIFICATION_TYPE = 2L;
		public static final Long  NONE = 3L;
		
		public static final Long  PIN_REGISTRATION_TYPE = 1L;
		public static final Long  VOTER_ID_REGISTRATION_TYPE = 2L;
		public static final Long  NIN_REGISTRATION_TYPE= 3L;
		public static final Long  PASSPORT_REGISTRATION_TYPE= 4L;
		public static final Long  NONE_REGISTRATION_TYPE= 5L;
		
		public static enum InformantType {
			FATHER("father"),MOTHER("mother");
			    
			    public String value;
			    public String getValue() {
			        return value;
			    }
			    public void setValue(String value) {
			        this.value = value;
			    }
			    private InformantType(String value){this.value = value;}
			}
		
		public static String VOTER_ID_VALIDATION_SERVER_ERROR = "voterIdValidationServerError";
		public static String NIN_VALIDATION_SERVER_ERROR = "ninValidationServerError";
		
		public static String CHILD_IN_VALID_PIN = "childInValidPin";
		public static String CHILD_IN_VALID_VOTER_ID = "childInValidVoterId";
		public static String CHILD_IN_VALID_NIN = "childInValidNIN";
		
		public static String MOTHER_IN_VALID_PIN = "motherInValidPin";
		public static String MOTHER_IN_VALID_VOTER_ID = "motherInValidVoterId";
		public static String MOTHER_IN_VALID_NIN = "motherInValidNIN";
		
		public static String MOTHER_IN_VALID_PIN_SEX = "motherInValidPinSex";
		public static String MOTHER_IN_VALID_VOTER_ID_SEX = "motherInValidVoterIdSex";
		public static String MOTHER_IN_VALID_NIN_SEX = "motherInValidNINSex";
		
		public static String FATHER_IN_VALID_PIN = "fatherInValidPin";
		public static String FATHER_IN_VALID_VOTER_ID = "fatherInValidVoterId";
		public static String FATHER_IN_VALID_NIN = "fatherInValidNIN";
		
		public static String FATHER_IN_VALID_PIN_SEX = "fatherInValidPinSex";
		public static String FATHER_IN_VALID_VOTER_ID_SEX = "fatherInValidVoterIdSex";
		public static String FATHER_IN_VALID_NIN_SEX = "fatherInValidNINSex";
		
		public static String DEACEASED_PERSON_IN_VALID_PIN = "deaceasedInValidPin";
		public static String DEACEASED_PERSON_IN_VALID_VOTER_ID = "deaceasedInValidVoterId";
		public static String DEACEASED_PERSON_IN_VALID_NIN = "deaceasedInValidNIN";
		
	}
	
	public static class BirthNotificationConstants {

		// Resource Bundle Constants
		public static String VIEW_BIRTH_NOTIFICATION  = "birthNotificationDetails";
		public static String IS_ADD_EDIT_BIRTH_NOTIFICATION = "isAddOrEditBirthNotification";
		public static String BIRTH_NOTIFICATION_PARAM = "birthNotificationId";
		public static String BIRTH_NOTIFICATION_NUMBER_PARAM = "birthNotificationNumber";
		
		public static String VIEW_DEATH_NOTIFICATION  = "deathNotificationDetails";
		public static String IS_ADD_EDIT_DEATH_NOTIFICATION = "isAddOrEditDeathNotification";
		public static String DEATH_NOTIFICATION_PARAM = "deathNotificationId";
		public static String DEATH_NOTIFICATION_NUMBER_PARAM = "deathNotificationNumber";
	}
	
	public static class DeviceManagementConstants {

		// Resource Bundle Constants
		public static String VIEW_DEVICE  = "deviceList";
		public static String IS_ADD_EDIT_DEVICE = "isAddOrEditDevice";
		public static String DEVICE_PARAM = "deviceId";
		public static String CENTER_TYPE_ID = "centerTypeId";
		public static String DEVICE_STATUS_ID = "deviceStatusId";
		public static String DEVICE_TYPE_ID= "deviceTypeId";
		
	}
	
	
	public static class TransactionManagement {

		// Resource Bundle Constants
		public static String ADD_TRANS_SUB_TYPE = "transSubTypeAddSuccess";
		public static String EDIT_TRANS_SUB_TYPE= "transSubTypeEditSuccess";
		public static String DELETE_TRANS_SUB_TYPE= "transSubTypeDeleteSuccess";
		public static String TRANS_SUB_TYPE_ALREADY_EXIST= "transSubTypeAlreadyExists";
		
		public static String ADD_DEATH_REGIS_TYPE = "deathAddSuccess";
		public static String EDIT_DEATH_REGIS_TYPE= "deathEditSuccess";
		public static String DELETE_DEATH_REGIS_TYPE= "deathDeleteSuccess";
		public static String DEATH_REGIS_ALREADY_EXIST= "deathAlreadyExists";
		
		public static String ADD_TRANS_TYPE_WAVE_FEE = "transTypeWaveFeeAddSuccess";
		public static String EDIT_TRANS_TYPE_WAVE_FEE= "transTypeWaveFeeEditSuccess";
		public static String EFFECTIVE_DATE_WAVE_FEE= "effectiveDate";
		public static String DELETE_TRANS_TYPE_WAVE_FEE= "transTypeWaveFeeDeleteSuccess";
		public static String IS_ADD_EDIT_TRANSACTION_TYPE = "isAddOrEditTransactionType";
		public static String IS_ADD_EDIT_TRANSACTION_TYPE_WAVE_FEE= "isAddOrEditTransactionTypeWaveFee";
		public static  String TRANSACTION_TYPE_PARAM  = "transactionId";
		public static  String TRANSACTION_SUB_TYPE_PARAM  = "transactionSubTypeId";
		public static String EFFECTIVE_FROM_WAVE_FEE= "effectiveFromDate";
		public static String EFFECTIVE_TO_WAVE_FEE= "effectiveToDate";
		public static  String TRANSACTION_TYPE_WAVE_FEE_PARAM  = "ttWaveFeeId";
		public static String IS_ADD_EDIT_BIRTH_TRANSACTION_TYPE = "isAddOrEditBirthTransactionType";
		public static String IS_ADD_EDIT_DEATH_TRANSACTION_TYPE = "isAddOrEditdeathTransactionType";
		public static String IS_ADD_EDIT_DEATH_TRANSACTION_TYPE_WAVE_FEE= "isAddOrEditDeathTransactionTypeWaveFee";
		public static  String TRANSACTION_BIRTH_TYPE_PARAM  = "birthTransactionId";
		public static  String TRANSACTION_DEATH_TYPE_PARAM  = "deathTransactionSubTypeId";
		public static String DAYS_RANGE_EXIST = "transactionDaysRangeExist";
		public static String FROM_DAYS_LESS_TO_DAYS = "daysDiffecrence";
		public static String WAVE_FEE_REQUIRED_LEVEL= "waiveFeeRequiredLevel";
		public static String WAVE_FEE_TIME_ALREADY_EXIST= "waiveFeeAlreadyExist";
		public static String WAVE_FEE_TIME_ALREADY_EXIST_INFINFTE_DATE= "waiveFeeAlreadyExistForUnlimitedTime";
		public static String VERIFY_TRANSACTION_NUMBER = "tansactionNumber";
		public static String IS_OFFLINE_BIRTH_VERIFY_PAYMENT = "isOfflineBirthVerifyPayment";
		public static String IS_OFFLINE_DEATH_VERIFY_PAYMENT = "isOfflineDeathVerifyPayment";
		
	}

	public static class WorkFlowManagementConstants {
		public static String ACTION_TYPE_PARAM = "actionTypeId";
		public static String STAGE_TYPE_PARAM = "stageTypeId";
		public static String STAGE_PARAM = "stageId";
		public static String WORKFLOW_PARAM = "workflowId";
		
		public static String IS_ADD_EDIT_WORKFLOW_PARAM = "isAddOrEditWorkflow";
		public static String IS_ADD_EDIT_STAGE_PARAM = "isAddOrEditStage";
		public static String IS_ADD_EDIT_STAGE_TYPE_PARAM = "isAddOrEditStageType";
		public static String IS_ADD_EDIT_ACTION_TYPE_PARAM = "isAddOrEditActionType";
		
		public static String WORKFLOW_ADD_TOKEN = "workflowAddedSuccessfully";
		public static String WORKFLOW_DELETE_TOKEN = "workflowDeletedSuccessfully";
		public static String WORKFLOW_UPDATE_TOKEN = "workflowUpdatedSuccessfully";
		public static String WORKFLOW_ALREADY_EXISTS_TOKEN ="workflowAlreadyExists";
		public static String WORKFLOW_STAGES_REQUIRED = "stagesRequired";
		public static String WORKFLOW_STAGES_MISSING_SEQUENCE="stagesMissingSequence";
		public static String WORKFLOW_STAGES_NOT_IN_SEQUENCE="stagesNotInSequence";
		public static String WORKFLOW_TRANS_TYPE_ALREADY_ATTACHED="transTypeAlreadyAttached";
		public static String WORKFLOW_STAGE_ALREADY_ADDED_IN_WORK_FLOW = "stageAlreadyAddedInWorkFlow";
		public static String WORKFLOW_LAST_STAGE_IS_NOT_COMPLETE="lastStageIsNotComplete";
		
		public static String ACTION_TYPE_ADD_TOKEN = "actionTypeAddedSuccessfully";
		public static String ACTION_TYPE_DELETE_TOKEN = "actionTypeDeletedSuccessfully";
		public static String ACTION_TYPE_UPDATE_TOKEN = "actionTypeUpdatedSuccessfully";
		public static String ACTION_TYPE_ALREADY_EXISTS_TOKEN ="actionTypeAlreadyExists"; 
		
		public static String STAGE_TYPE_ADD_TOKEN = "stageTypeAddedSuccessfully";
		public static String STAGE_TYPE_DELETE_TOKEN = "stageTypeDeletedSuccessfully";
		public static String STAGE_TYPE_UPDATE_TOKEN = "stageTypeUpdatedSuccessfully";
		public static String STAGE_TYPE_ALREADY_EXISTS_TOKEN ="stageTypeAlreadyExists";
		
		public static String STAGE_ADD_TOKEN = "stageAddedSuccessfully";
		public static String STAGE_DELETE_TOKEN = "stageDeletedSuccessfully";
		public static String STAGE_UPDATE_TOKEN = "stageUpdatedSuccessfully";
		public static String STAGE_ALREADY_EXISTS_TOKEN ="stageAlreadyExists";
		
		public static String DYNAMIC_REPORT_PARAM = "dynamicReportId";
		public static String IS_ADD_EDIT_DYNAMIC_REPORT_PARAM = "isAddOrEditDynamicReport";
	}
	
	public static class DDConstants {
		public static final String EMPTY_STRING = "";
		public static final String REGION_DELETE_TOKEN = "RegionDeletedSuccessfully";
		public static final boolean FALSE = false;

		public static final String ADD_ALL_ITEM = "All";
		public static final String REGION_PARAM = "regionID";
		public static final String IS_ADD_EDIT_REGION_PARAM = "isAddOrEditRegionType";
		public static final String ADD_EDIT_REGION_PAGE = "addEditRegions";
		public static String REGION_UPDATE_TOKEN = "RegionUpdatedSuccessfully";
		public static final String REGION_ADD_TOKEN = "RegionAddedSuccessfully";
		public static final String REGION_ALREADY_EXISTS_TOKEN ="RegionAlreadyExists";
		public static final String POSTCODE_ALREADY_EXISTS_TOKEN="PostCodeAlreadyExits";
		public static final String DEACTIVATE_REGION_CHILDREN="inactiveChild";
		
		public static final String DISTRICT_PARAM = "districtID";
		public static String IS_ADD_EDIT_DISTRICT_PARAM = "isAddOrEditDistrictType";
		public static final String DISTRICT_DELETE_TOKEN = "DistrictDeletedSuccessfully";
		public static final String ADD_EDIT_DISTRICT_PAGE = "addEditDistricts";
		public static String DISTRICT_UPDATE_TOKEN = "DistrictUpdatedSuccessfully";
		public static final String DISTRICT_ADD_TOKEN = "DistrictAddedSuccessfully";
		public static final String DISTRICT_ALREADY_EXISTS_TOKEN ="DistrictAlreadyExists";
		public static final String DEACTIVATE_DISTRICT_CHILDREN="inactiveDistrictChild";
		
		public static final String COUNCIL_PARAM = "councilID";
		public static String IS_ADD_EDIT_COUNCIL_PARAM = "isAddOrEditCouncilType";
		public static final String COUNCIL_DELETE_TOKEN = "CouncilDeletedSuccessfully";
		public static final String ADD_EDIT_COUNCIL_PAGE = "addEditCouncils";
		public static String COUNCIL_UPDATE_TOKEN = "CouncilUpdatedSuccessfully";
		public static final String COUNCIL_ADD_TOKEN = "CouncilAddedSuccessfully";
		public static final String COUNCIL_ALREADY_EXISTS_TOKEN ="CouncilAlreadyExists";
		public static final String DEACTIVATE_COUNCIL_CHILDREN="inactiveCouncilChild";
		
		public static final String WARD_PARAM = "wardID";
		public static String IS_ADD_EDIT_WARD_PARAM = "isAddOrEditWardType";
		public static final String WARD_DELETE_TOKEN = "WardDeletedSuccessfully";
		public static final String ADD_EDIT_WARD_PAGE = "addEditWards";
		public static String WARD_UPDATE_TOKEN = "WardUpdatedSuccessfully";
		public static final String WARD_ADD_TOKEN = "WardAddedSuccessfully";
		public static final String WARD_ALREADY_EXISTS_TOKEN ="WardAlreadyExists";
		public static final String DEACTIVATE_WARD_CHILDREN="inactiveRegistrationCenterChild";
		
		public static final String HFACILITY_PARAM = "HealthFacilityID";
		public static String IS_ADD_EDIT_HFACILITY_PARAM = "isAddOrEditHealthFacilityType";
		public static final String HFACILITY_DELETE_TOKEN = "HealthFacilityDeletedSuccessfully";
		public static final String ADD_EDIT_HFACILITY_PAGE = "addEditHealthFacility";
		public static String HFACILITY_UPDATE_TOKEN = "HealthFacilityUpdatedSuccessfully";
		public static final String HFACILITY_ADD_TOKEN = "HealthFacilityAddedSuccessfully";
		public static final String HFACILITY_ALREADY_EXISTS_TOKEN ="HealthFacilityAlreadyExists";
		public static final String PRIVILIGE_NOT_EXIST ="priviligeNotExists";
		public static final String DEACTIVATE_HF_CHILDREN="inactiveHFRegistrationCenterChild";
	}

	public static class Notification {
	    public static final String VOTER_ID = "V";
	    public static final String NIN = "I";
	    public static final String PIN = "P";
	    
	    public static final String MALE = "M";
	    public static final String FEMALE = "F";
	    
	    public static final String HOME = "N";
	    public static final String HEALTH_FACILITY = "H";
	    public static final String OTHER = "O"; 
	    
	    
	    public static final Long NATURAL = 1L;
	    public static final Long UNNATURAL = 2L;
	    
	    public static class Relations{
		public static final String MOTHER = "M";
		public static final String FATHER = "F";
		public static final String SELF = "S";
		public static final String OTHER = "Y"; 
		public static final String HEALTH_FACILITY = "T";
		public static final String BROTHER = "B";
		public static final String SISTER = "S";
		public static final String SON = "N";
		public static final String DAUGHTER = "D";
	    }
	    
	    public static enum RelationsName{
		FATHER("Father"),MOTHER("Mother"),SELF("Self"), OTHER("Other"), HEALTH_FACILITY("Health Facility"),
		BROTHER("Brother"),SISTER("Sister"),SON("Son"), DAUGHTER("Daughter"), INFORMANT("Informant") ;
		    
		    public String value;
		    public String getValue() {
		        return value;
		    }
		    public void setValue(String value) {
		        this.value = value;
		    }
		    private RelationsName(String value){this.value = value;}
		}
	    
	    public static enum PlaceOfDeathName{
		HOME("Home"),HEALTH_FACILITY("Health Facility"),OTHER("Other");
		    
		    public String value;
		    public String getValue() {
		        return value;
		    }
		    public void setValue(String value) {
		        this.value = value;
		    }
		    private PlaceOfDeathName(String value){this.value = value;}
		} 
	    public static enum Sex {
		 MALE("Male"),FEMALE("Female");
		    
		 public String value;
		 public String getValue() {
		        return value;
		 }
		 public void setValue(String value) {
		        this.value = value;
		 }
		 private Sex(String value){this.value = value;}
	    }
	}
	public static class Relations{
	    
	    public static final Long SELF = 1L;
	    public static final Long MOTHER = 2L;
	    public static final Long FATHER = 3L;
	    public static final Long OTHER = 4L; 
	    public static final Long BROTHER = 5L;
	    public static final Long SISTER = 6L;
	    public static final Long SON = 7L;
	    public static final Long DAUGHTER = 8L;
	    
	} 
	public static class CertificatePrintRequestType{
	    
	    public static final Long BIRTH = 1L;
	    public static final Long DEATH = 2L;
	     
	} 
	
	public static class CenterTypeConstant{
		public static final String Add_All_Item="All";
		public static final String Add_Birth_Item="Birth";
		public static final String Add_Death_Item="Death";
		public static final String Add_Normal_Item="Normal";
		public static final String Add_Late_Item="Late";
		public static final String Duplicate_Printing="Duplicate Printing";
		public static final String Birth_Modification="Birth Modification";
		public static final String Death_Modification="Death Modification";
	}
	public static class DateConstraints{
		public static final String Date_Range_Required="dateRangeRequired";
		public static final String Invalid_Date_Range="invalidDateRequired";
	}
	
	public static class ReRegistration{
	    public static final String BIRTH_PLACE_HOSPITALS = "H";
	    public static final String BIRTH_PLACE_HOME = "N";
	    public static final String BIRTH_PLACE_ANY_OTHER_ELSEWHERE  = "M";
	     
	    public static final String BIRTH_STATUS_LIVE = "M";
	    public static final String BIRTH_STATUS_DEAD = "H";

	    public static final String BIRTH_TYPE_ONE = "1";
	    public static final String BIRTH_TYPE_TWINS = "2";
	    public static final String BIRTH_TYPE_MORE_THAN_TWO = "9";
	     
	    public static final String BIRTH_TYPE_SINGLE_CHAR = "S";
	    public static final String BIRTH_TYPE_TWINS_CHAR = "T";
	    public static final String BIRTH_TYPE_OTHER_CHAR = "O";
	    
	    public static final String BIRTH_TYPE_SINGLE = "SINGLE";
	    public static final String BIRTH_TYPE_TWINS_STRING = "TWINS" ;
	    public static final String BIRTH_TYPE_TRIPLETS = "TRIPLETS";
	    
	    public static final String INFORMANT_TYPE_FATHER = "B";
	    public static final String INFORMANT_TYPE_MOTHER = "M";
	    public static final String INFORMANT_TYPE_HEATH_FACILITY = "T";
	    public static final String INFORMANT_TYPE_ANY_OTHER = "Y";
	    
	    public static final String MALE = "MALE";
	    public static final String FEMALE = "FEMALE";
	
	    public static final String FATHER = "B";
	    public static final String MOTHER = "M";
	    public static final String HEALTH_FACILITY = "T";
	    public static final String ANY_OTHER = "Y";
	    
	    public static enum TypeOfBirthEnum {
		 ONE("one"),TWINS("twins"),MORE_THAN_TWO("More than two"), SINGELE("SINGLE");
		    
		 public String value;
		 public String getValue() {
		        return value;
		 }
		 public void setValue(String value) {
		        this.value = value;
		 }
		 private TypeOfBirthEnum(String value){this.value = value;}
	    }
	    public static enum InformantType {
		FATHER("Father"),MOTHER("Mother"),HEALTH_FACILITY("Health Facility"), ANY_OTHER("Any other"), OTHERS("others");
		    
		    public String value;
		    public String getValue() {
		        return value;
		    }
		    public void setValue(String value) {
		        this.value = value;
		    }
		    private InformantType(String value){this.value = value;}
		}
	}	
	
	public static class TypeOfBirth{
	    public static final Long SINGLE = 1L;
	    public static final Long TWIN = 2L;
	    public static final Long TRIPLET = 3L;
	    public static final Long OTHER = 4L; 
	} 
	
	public static class DeathReportsConstraints{
		public static final String AGE_DIFFERENCE="ageDiffernce";
		//public static final String Invalid_Date_Range="invalidDateRequired";
	}

	public static class BirthReportsConstraints{
		public static final String Date_Range_Required="dateRangeRequired";
		//public static final String Invalid_Date_Range="invalidDateRequired";
	}
}
