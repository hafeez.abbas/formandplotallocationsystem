package com.infotech.fapas.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.infotech.fapas.model.User;

@Repository("userDAO")
@Scope("singleton")
public class UserDAO extends BaseDAO<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3139756213611339437L;
	
	public UserDAO(){
		super(User.class);
	}

}
