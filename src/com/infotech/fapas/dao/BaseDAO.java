package com.infotech.fapas.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unchecked")
@Repository("baseDAO")
@Scope("singleton")
public class BaseDAO<E> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1531765151252714287L;

	@PersistenceContext
	protected EntityManager entityManager;
	private Class<E> type;

	public BaseDAO() {

	}

	public BaseDAO(Class<E> type) {
		this.type = type;
	}

	public Session getSession(){
		return (Session) this.entityManager.getDelegate();
	}
	
	public void save(Object obj) {
		try {
			getSession().save(obj);
		} catch (HibernateException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void update(Object obj) {
		try {
			getSession().update(obj);
		} catch (HibernateException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void delete(Object obj) {
		try {
			getSession().delete(obj);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public Object merge(Object object){
		try {
			return getSession().merge(object);
		} catch (HibernateException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void saveOrUpdate(Object obj) {
		try {
			getSession().saveOrUpdate(obj);
		} catch (HibernateException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public E findById(Class<E> type2,Long id) {
		try {
			return (E)getSession().get(type2, id);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public E findById(Integer id) {
		try {
			return (E)getSession().get(type, id);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public List<E> findAll() {
		try {
			String queryString = "Select t from " + type.getSimpleName() + " t";
			Query query = getSession().createQuery(queryString);
			return query.list();
			
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List<E> findByProperty(Class<E> type,Object property,Object value) {
		try {
			Query query = getSession().createQuery("Select t from "+type.getSimpleName()+ " t where t."+property+" = :value");
			query.setParameter("value", value);
			return (List<E>)query.list();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	public List<E> findByProperty(Class<E> type,String property,Long value) {
		try {
			Query query = getSession().createQuery("Select t from "+type.getSimpleName()+ " t where t."+property+" = :value");
			query.setParameter("value", value);
			return (List<E>)query.list();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	public List<E> findByProperty(Class<E> type,String property,String value) {
		try {
			Query query = getSession().createQuery("Select t from "+type.getSimpleName()+ " t where UPPER(TRIM(t."+property+")) = :value");
			query.setParameter("value", value.trim().toUpperCase());
			return (List<E>)query.list();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
