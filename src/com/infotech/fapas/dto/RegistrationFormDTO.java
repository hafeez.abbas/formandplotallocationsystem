package com.infotech.fapas.dto;

import java.io.Serializable;

public class RegistrationFormDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -856765887057918111L;
	
	private String fileType;
	private String area;
	private String serialNo;
	private String applicantCategory;
	private String applicantCnic;
	private String firstName;
	private String middleName;
	private String lastName;
	private String postalAddress;
	private String city;
	private String province;
	private String telephone;
	private String email;
	private String nomineeCnic;
	private String nomineeName;
	private String nomineePhone;
	private String nomineeRelation;
	private String bank;
	private String branch;
	private String intrumentNo;
	private int amount;
	private String receiptNumber;
	private byte[] formAttachment = null;
	private byte[] receiptAttachment = null;
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getApplicantCategory() {
		return applicantCategory;
	}
	public void setApplicantCategory(String applicantCategory) {
		this.applicantCategory = applicantCategory;
	}
	public String getApplicantCnic() {
		return applicantCnic;
	}
	public void setApplicantCnic(String applicantCnic) {
		this.applicantCnic = applicantCnic;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomineeCnic() {
		return nomineeCnic;
	}
	public void setNomineeCnic(String nomineeCnic) {
		this.nomineeCnic = nomineeCnic;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	public String getNomineePhone() {
		return nomineePhone;
	}
	public void setNomineePhone(String nomineePhone) {
		this.nomineePhone = nomineePhone;
	}
	public String getNomineeRelation() {
		return nomineeRelation;
	}
	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getIntrumentNo() {
		return intrumentNo;
	}
	public void setIntrumentNo(String intrumentNo) {
		this.intrumentNo = intrumentNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public byte[] getFormAttachment() {
		return formAttachment;
	}
	public void setFormAttachment(byte[] formAttachment) {
		this.formAttachment = formAttachment;
	}
	public byte[] getReceiptAttachment() {
		return receiptAttachment;
	}
	public void setReceiptAttachment(byte[] receiptAttachment) {
		this.receiptAttachment = receiptAttachment;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
