package com.infotech.fapas.manager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.infotech.fapas.dao.UserDAO;
import com.infotech.fapas.model.User;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

@Component("userManager")
@Transactional
@Scope("singleton")
public class UserManagerImpl implements Serializable, UserManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8611762104527920184L;
	@Autowired
	UserDAO userDao;
	
	@Override
	public void saveUser(User user) {
		userDao.save(user);
		
	}

	@Override
	public void updateUser(User user) {
		userDao.update(user);
		
	}

	@Override
	public User getUser(Integer id) {
		return userDao.findById(id);
	}

}
