package com.infotech.fapas.manager;

import com.infotech.fapas.model.User;

public interface UserManager {
	public void saveUser(User user);
	public void updateUser(User user);
	public User getUser(Integer id);
}
