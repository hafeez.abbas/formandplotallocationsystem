package com.infotech.fapas.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="users")
public class User {
	private Integer id;
	private String userName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String password;
	private String email;
	private String telephoneNumber;
	private String fax;
	private String mobile;
	private String postalAddress;
	private String active;
	private String lock;
	private String userChangedPassword;
	private Date passwordExpiresOn;
	private Date lastLoggedInDate;
	private String loggedIn;
	private Integer loginAttempts;
	private Integer orgId;
	private Integer roleId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "USER_NAME", nullable = true)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "FIRST_NAME", nullable = true)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name = "MIDDLE_NAME", nullable = true)
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	@Column(name = "LAST_NAME", nullable = true)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name = "PASSWORD", nullable = true)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "EMAIL", nullable = true)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "TELEPHONE_NUMBER", nullable = true)
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	
	@Column(name = "FAX", nullable = true)
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	@Column(name = "MOBILE", nullable = true)
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Column(name = "POSTAL_ADDRESS", nullable = true)
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	
	@Column(name = "ACTIVE", nullable = true,columnDefinition = "char")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	@Column(name = "USER_LOCK", nullable = true,columnDefinition = "char")
	public String getLock() {
		return lock;
	}
	public void setLock(String lock) {
		this.lock = lock;
	}
	
	@Column(name = "USER_CHANGED_PASSWORD", nullable = true,columnDefinition = "char")
	public String getUserChangedPassword() {
		return userChangedPassword;
	}
	public void setUserChangedPassword(String userChangedPassword) {
		this.userChangedPassword = userChangedPassword;
	}
	
	@Column(name = "PASSWORD_EXPIRES_ON", nullable = false)
	public Date getPasswordExpiresOn() {
		return passwordExpiresOn;
	}
	public void setPasswordExpiresOn(Date passwordExpiresOn) {
		this.passwordExpiresOn = passwordExpiresOn;
	}
	
	@Column(name = "LAST_LOGGEDIN_DATE", nullable = false)
	public Date getLastLoggedInDate() {
		return lastLoggedInDate;
	}
	public void setLastLoggedInDate(Date lastLoggedInDate) {
		this.lastLoggedInDate = lastLoggedInDate;
	}
	
	@Column(name = "LOGIN_ATTEMPTS", nullable = true)
	public Integer getLoginAttempts() {
		return loginAttempts;
	}
	public void setLoginAttempts(Integer loginAttempts) {
		this.loginAttempts = loginAttempts;
	}
	
	@Column(name = "ORG_ID", nullable = true)
	public Integer getOrgId() {
		return orgId;
	}
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity = Role.class)
	@JoinColumn(name="ROLE_ID", nullable = true)
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	@Column(name = "LOGGED_IN", nullable = true,columnDefinition = "char")
	public String getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(String loggedIn) {
		this.loggedIn = loggedIn;
	} 

}
