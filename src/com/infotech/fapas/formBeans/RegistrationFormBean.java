package com.infotech.fapas.formBeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.event.FileUploadEvent;

import com.infotech.fapas.dto.RegistrationFormDTO;
@ManagedBean(name = "registrationFormBean")
@ViewScoped
public class RegistrationFormBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3197075855249917173L;
	private RegistrationFormDTO registrationFormDTO;
	private byte[] formAttachment = null;
	private byte[] receiptAttachment = null;
	private String displayPicture ;
	private boolean isEdit = false;
	private boolean isShowUploadPanel = false;
	
	public  RegistrationFormBean() {
		// TODO Auto-generated constructor stub
	}
 
	@PostConstruct
	public void init(){
		
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		try {
			formAttachment = event.getFile().getContents();
			displayPicture = Base64.encodeBase64String(formAttachment);
		} catch (RuntimeException e) {
			//ApplicationMessagingService.addSystemExceptionMessage(e);
		}
	}
	public boolean isShowUpload() {
		boolean result = true;
		if (getFormAttachment() != null && !isShowUploadPanel) {
			result = false;
		}

		return result;
	}

	public void showUploadPanel() {

	}
	
	public void setShowUploadPanel(boolean isShowUploadPanel) {
		this.isShowUploadPanel = isShowUploadPanel;
	}

	public void hideShowUploadPanel(boolean value) {
		setShowUploadPanel(value);
	}
	
	public void hideCancelButton(boolean value) {
		setShowUploadPanel(value);
		this.formAttachment=null;
	}

	public RegistrationFormDTO getRegistrationFormDTO() {
		return registrationFormDTO;
	}
	public void setRegistrationFormDTO(RegistrationFormDTO registrationFormDTO) {
		this.registrationFormDTO = registrationFormDTO;
	}

	public byte[] getFormAttachment() {
		return formAttachment;
	}

	public void setFormAttachment(byte[] formAttachment) {
		this.formAttachment = formAttachment;
	}

	public byte[] getReceiptAttachment() {
		return receiptAttachment;
	}

	public void setReceiptAttachment(byte[] receiptAttachment) {
		this.receiptAttachment = receiptAttachment;
	}

	public String getDisplayPicture() {
		return displayPicture;
	}

	public void setDisplayPicture(String displayPicture) {
		this.displayPicture = displayPicture;
	}

	public boolean isEdit() {
		return isEdit;
	}

	public void setEdit(boolean isEdit) {
		this.isEdit = isEdit;
	}

	public boolean isShowUploadPanel() {
		return isShowUploadPanel;
	}

}
