package com.infotech.fapas.formBeans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name = "userSessionBean")
@SessionScoped
public class UserSessionBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5515064636761032052L;
	private Map<String, Object> parameterMap = new HashMap<String,Object>();
	private Integer sessionTimeOut;
	private boolean isFromLogin = true;
	
	public UserSessionBean() {
		
	}

	public String navigateToPage(String pageToNavigate, boolean resetMap){
		if(resetMap){
			parameterMap.clear();
		}
		return pageToNavigate + com.infotech.fapas.constants.NavigationConstants.FACES_REDIRECT;
	}
	
	public void clearParamMap(){
		parameterMap.clear();
	}
	public String navigateFromMenu(String pageToNavigate){
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("fromMenu", true);
		return pageToNavigate;
	}
	public String navigateToDashboard(){
		String outcome = com.infotech.fapas.constants.GUIConstants.DASHBOARD_OUTCOME;
		return outcome;
	}
	
	public String navigateToLogin(){
		return com.infotech.fapas.constants.GUIConstants.BACK_TO_LOGIN_OUTCOME;
		
	}
		
	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public Integer getSessionTimeOut() {
		return sessionTimeOut;
	}

	public void setSessionTimeOut(Integer sessionTimeOut) {
		this.sessionTimeOut = sessionTimeOut;
	}

	public boolean isFromLogin() {
		return isFromLogin;
	}

	public void setFromLogin(boolean isFromLogin) {
		this.isFromLogin = isFromLogin;
	}
	
}
