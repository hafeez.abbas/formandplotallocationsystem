function printDiv(divToPrintId){
								var divToPrint = document.getElementById(divToPrintId);
								var newWin = window.open('','Print-Window');
								var logoSrc = $('#layout-logo').children()[0].attributes.src.value
								newWin.document.open();
								newWin.document.write('<html><head><link rel="stylesheet" href="/rita-crvs/javax.faces.resource/theme.css.xhtml?ln=primefaces-sentinel" type="text/css"/>');
								newWin.document.write('<link rel="stylesheet" href="'+'/rita-crvs/javax.faces.resource/primefaces.css.xhtml?ln=primefaces&amp;v=5.3'+'" type="text/css"/>');
								newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/css/font-icon-layout.css.xhtml?ln=sentinel-layout" type="text/css"/>');
								newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/css/sentinel-layout.css.xhtml?ln=sentinel-layout" type="text/css"/>');
								newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/css/core-layout.css.xhtml?ln=sentinel-layout" type="text/css"/>');								
								newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/ritaCustomStyles.css.xhtml?ln=css" type="text/css"/> </head>');
								newWin.document.write('<body>'+divToPrint.innerHTML);
								newWin.document.write('</body>');								
								newWin.document.write('</html>');
								newWin.document.close(); // necessary for IE >= 10
								newWin.focus(); // necessary for IE >= 10
								setTimeout(function () { // necessary for Chrome
							    	newWin.print();
							    	newWin.close();
							    }, 1000);
							}
function printDivWithLogo(divToPrintId){
	var divToPrint = document.getElementById(divToPrintId);
	var newWin = window.open('','Print-Window');
	var logoSrc = $('#layout-logo').children()[0].attributes.src.value;
	var ritaHeader = $('#layout-logo').children()[1];
	newWin.document.open();
	newWin.document.write('<html><head>');
	newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/ritaProfileStyles.css.xhtml?ln=css" type="text/css"/> </head>');
	newWin.document.write('<body><div class="Wid100 TexAlLeft" ><img src="'+logoSrc +'" class="Fleft" style="width:80px" />'+ritaHeader.innerHTML.replace(/Fs40/gi,'Fs20 Fleft MarTop30 MarLeft10 FontTextBold')+divToPrint.innerHTML+'</div>');
	newWin.document.write('</body>');								
	newWin.document.write('</html>');
	newWin.document.close(); // necessary for IE >= 10
	newWin.focus(); // necessary for IE >= 10
	setTimeout(function () { // necessary for Chrome
    	newWin.print();
    	newWin.close();
    }, 1000);
}

function printBirthCertificate(divToPrintId){
	
	$('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']").parent().parent().css('overflow','visible')
	if(navigator.userAgent.toLowerCase().match("edge") ){
		$($('.jrPage')[0]).css('margin-left','39.5%');
		$($('.jrPage')[0]).css('margin-top','27.6%');
		$($('.jrPage')[0]).css('width','760px');
		$($('.jrPage')[0]).css("-webkit-transform","scale(1.58)");
	}else if(navigator.userAgent.toLowerCase().match("firefox") ){
		$($('.jrPage')[0]).css('margin-left','48.5%');
		$($('.jrPage')[0]).css('margin-top','77.15%');
		$($('.jrPage')[0]).css('width','662px');
		$($('.jrPage')[0]).css("-webkit-transform","scale(1.78)");
	}else{
		$($('.jrPage')[0]).css('width','740px');
		$($('.jrPage')[0]).css('height','200px');
		$($('.jrPage')[0]).css('margin-top','2%');
		$($('.jrPage')[0]).css('margin-left','3.5%');
	}
	
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[2]).parent().css({"-webkit-transform":"translate(-82px,75px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[3]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[4]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[5]).parent().css({"-webkit-transform":"translate(-82px,75px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[6]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[7]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[8]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[9]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[10]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[11]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[12]).parent().css({"-webkit-transform":"translate(-68px,60px) rotate(-90deg)"})
	
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[0]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[1]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[2]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[12]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[4]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[5]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[6]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[16]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[18]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[13]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[10]).parent().css('top','1.25px')
	
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[2]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[3]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[4]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[5]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[6]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[7]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[8]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[9]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[10]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[11]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[12]).children().css('word-break','break-word')

	$('#detailsForm\\:popUpHiddenValue').val('true');
	$('#detailsForm\\:popUpHiddenValue').trigger('change');
	
	var divToPrint = $(divToPrintId).parent()[0];
	var innerHTML = divToPrint.innerHTML;
	var newWin = window.open('','Print-Window');
	try{
		newWin.document.write('<html><head>');
		newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/ritaCertificateStyles.css.xhtml?ln=css" type="text/css"/> </head>');
		if(navigator.userAgent.toLowerCase().match("edge") ){
			newWin.document.write('<body> '+innerHTML);
			newWin.document.write('</body>');
		}
		else
		{
			newWin.document.write('<body style="width:8in !important;height:3in !important"> '+innerHTML);
			newWin.document.write('</body>');
		}
		
		newWin.document.write('</html>');
		newWin.document.close(); // necessary for IE >= 10
		newWin.focus(); // necessary for IE >= 10
		setTimeout(function () { // necessary for Chrome
	    	newWin.print();
		    newWin.close();
	    }, 1000);
	}catch(e){
		$('#detailsForm\\:popUpHiddenValue').val('false');
		$('#detailsForm\\:popUpHiddenValue').trigger('change');
	}
}

function printDeathCertificate(divToPrintId){
	
	$('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']").parent().parent().css('overflow','visible')
	$('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']").parent().parent().css('overflow','visible')
	if(navigator.userAgent.toLowerCase().match("edge") ){
			$($('.jrPage')[0]).css('margin-left','40.48%');
			$($('.jrPage')[0]).css('margin-top','24%');
			$($('.jrPage')[0]).css('width','640px');
			$($('.jrPage')[0]).css('height','200px');
			$($('.jrPage')[0]).css("-webkit-transform","scale(1.82)");
		}else if(navigator.userAgent.toLowerCase().match("firefox") ){
			$($('.jrPage')[0]).css('margin-left','39.5%');
			$($('.jrPage')[0]).css('margin-top','71.5%');
			$($('.jrPage')[0]).css('width','652px');
			$($('.jrPage')[0]).css("-webkit-transform","scale(1.74)");
		}else{
			$($('.jrPage')[0]).css('margin-top','11.5%');
			$($('.jrPage')[0]).css('margin-left','8%');
			$($('.jrPage')[0]).css('width','640px');
			$($('.jrPage')[0]).css('height','200px');
			$($('.jrPage')[0]).css("-webkit-transform","scale(1.09)");
		}
	
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[2]).parent().css({"-webkit-transform":"translate(-81px,77px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[6]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[4]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[3]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[5]).parent().css({"-webkit-transform":"translate(-75px,69px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[7]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[8]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[9]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[12]).parent().css({"-webkit-transform":"translate(-75px,69px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[10]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[11]).parent().css({"-webkit-transform":"translate(-75px,70px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[13]).parent().css({"-webkit-transform":"translate(-95px,92px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[14]).parent().css({"-webkit-transform":"translate(-81px,77px) rotate(-90deg)"})
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[15]).children().css('font-size','0.275cm')
	
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[1]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[2]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[3]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[4]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[5]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[6]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[7]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[8]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[9]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[10]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[11]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[12]).parent().css('top','1.25px')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:top;']")[13]).parent().css('top','1.25px')
	
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[2]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[6]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[4]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[3]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[5]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[7]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[8]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[9]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[12]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[10]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[11]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[13]).children().css('word-break','break-word')
	$($('.jrPage').find("span[style='display:table-cell;vertical-align:middle;']")[14]).children().css('word-break','break-word')
	
	$('#detailsForm\\:popUpHiddenValue').val('true');
	$('#detailsForm\\:popUpHiddenValue').trigger('change');
	
	var divToPrint = $(divToPrintId).parent()[0];
	var innerHTML = divToPrint.innerHTML;
	var newWin = window.open('','Print-Window');
	try{
		newWin.document.write('<html><head>');
		newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/ritaCertificateStyles.css.xhtml?ln=css" type="text/css"/> </head>');
		if(navigator.userAgent.toLowerCase().match("edge") ){
			newWin.document.write('<body style="width:8in !important;height:3in !important"> '+innerHTML);
			newWin.document.write('</body>');
		}
		else
		{
			newWin.document.write('<body style="width:8in !important;height:3in !important"> '+innerHTML);
			newWin.document.write('</body>');
		}
		newWin.document.write('</html>');
		newWin.document.close(); // necessary for IE >= 10
		newWin.focus(); // necessary for IE >= 10
		setTimeout(function () { // necessary for Chrome
	    	newWin.print();
	    	newWin.close();
	    }, 1000);
	}catch(e){
		$('#detailsForm\\:popUpHiddenValue').val('false');
		$('#detailsForm\\:popUpHiddenValue').trigger('change');
	}
}


function printReceipt(divToPrintId){
	var divToPrint = document.getElementById(divToPrintId);	
	var newWin = window.open('','Print-Receipt');
	var logoSrc = $('#layout-logo').children()[0].attributes.src.value;
	newWin.document.open();
	newWin.document.write('<html moznomarginboxes mozdisallowselectionprint><head>');
	newWin.document.write('<link rel="stylesheet" href="/rita-crvs/javax.faces.resource/ritaReceiptStyles.css.xhtml?ln=css" type="text/css"/> </head>');
	newWin.document.write('<body style="width:4.5in !important; height:5.5in !important; margin:auto !important;>'+'<div id="printScreen" name="printScreen">'+divToPrint.innerHTML);
	newWin.document.write('</div></body>');
	newWin.document.write('</html>');
	newWin.document.close(); // necessary for IE >= 10
	newWin.focus(); // necessary for IE >= 10
    setTimeout(function () { // necessary for Chrome
    	newWin.print();
    	newWin.close();
    }, 1000);
}

